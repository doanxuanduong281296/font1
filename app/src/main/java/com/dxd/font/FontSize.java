package com.dxd.font;

public class FontSize {
    private int numA;
    private String numB;
    private int numC;
    private boolean blx;
    private boolean blClick;

    public FontSize(int numA, String numB, int numC, boolean blx, boolean blClick) {
        this.numA = numA;
        this.numB = numB;
        this.numC = numC;
        this.blx = blx;
        this.blClick = blClick;
    }

    public boolean isBlClick() {
        return blClick;
    }

    public void setBlClick(boolean blClick) {
        this.blClick = blClick;
    }

    public int getNumA() {
        return numA;
    }

    public void setNumA(int numA) {
        this.numA = numA;
    }


    public String getNumB() {
        return numB;
    }

    public void setNumB(String numB) {
        this.numB = numB;
    }

    public int getNumC() {
        return numC;
    }

    public void setNumC(int numC) {
        this.numC = numC;
    }

    public boolean isBlx() {
        return blx;
    }

    public void setBlx(boolean blx) {
        this.blx = blx;
    }
}
