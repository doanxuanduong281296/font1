package com.dxd.font;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddFont_Act extends AppCompatActivity {
    private SeekBar seekBar;
    private TextView tvFontSize;
    int i = 0;
    private String b ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_font_main);
        initViews();
    }

    private void initViews() {
        tvFontSize = findViewById(R.id.tv_text_font_size);
        findViewById(R.id.iv_back_add_font).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddFont_Act.this, MainAct.class);
                startActivity(intent);
                finish();
            }
        });
        seekBar= findViewById(R.id.seekbar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Aa 100%
                tvFontSize.setText("Aa "+progress+"%");
                tvFontSize.setTextSize((progress/10) +12);
                i= progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        findViewById(R.id.bt_new_setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkList()|| Settings.System.canWrite(getApplicationContext())){
                    saveFont();

                    showDialogSuccess();
                } else if (checkList() && !Settings.System.canWrite(getApplicationContext())){
                    AlertDialog.Builder alDialog = new AlertDialog.Builder(AddFont_Act.this);
                    alDialog.setTitle("Thông báo!");
                    alDialog.setIcon(R.mipmap.ic_launcher);
                    alDialog.setMessage("Bạn cần cấp quyền cho thay đổi này!");
                    alDialog.setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            checkSystemWritePermission();
                            dialog.dismiss();
                        }
                    });
                    alDialog.setNegativeButton("Từ chối", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alDialog.show();

                }
            }
        });
    }


    private boolean checkList(){
        String txData = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE).getString(MainAct.KEY_DATA, null);
        if (txData != null) {
            FontSize[] arrAlarm = new Gson().fromJson(txData, FontSize[].class);
            if (arrAlarm.length == 6){
                return true;
            } else if (arrAlarm.length >6){
                return false;
            }
        }
        return false;

    }

    private boolean checkSystemWritePermission() {
        boolean retVal = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            retVal = Settings.System.canWrite(this);
            Log.d("TAG", "Can Write Settings: " + retVal);
           if (!retVal){
               openAndroidPermissionsMenu();
           }
        }
        return retVal;
    }
    private void openAndroidPermissionsMenu() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + this.getPackageName()));
        startActivity(intent);
    }
    private void saveFont(){
        if (i > 100&& i<200){
            int a= (i-100)/10;
            b= "1."+a;
            saveSharedPreferences();
        } else if (i == 200) {
            b = 2+".0";
            saveSharedPreferences();
        } else if (i> 200){
            int a= (i-200)/10;
             b = "2."+ a;
            saveSharedPreferences();
        } else if (i<100){
            if (i == 0) {

            } else {
                int a= i/10;
                b = "0."+ a;
                saveSharedPreferences();
            }
        }

    }

    private void saveSharedPreferences() {
        FontSize fontSize = new FontSize(i, b, 12+(i/10), true, true);
        List<FontSize> listData = new ArrayList<>();
        String txData = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE).getString(MainAct.KEY_DATA, null);
        FontSize[] arrAlarm = new Gson().fromJson(txData, FontSize[].class);
        listData.addAll(Arrays.asList(arrAlarm));
        for (int j = 0; j <listData.size() ; j++) {
            if (i == listData.get(j).getNumA()){
                return;
            }
            listData.get(j).setBlx(false);
        }
        listData.add(fontSize);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
        pref.edit().putString(MainAct.KEY_DATA, new Gson().toJson(listData)).apply();
    }


    private void showDialogSuccess() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView imBackdialog = dialog.findViewById(R.id.iv_back_dialog);
        imBackdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(AddFont_Act.this, MainAct.class);
                startActivity(intent);
                List<FontSize> listData3 = new ArrayList<>();
                String txData = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE).getString(MainAct.KEY_DATA, null);
                FontSize[] arrAlarm = new Gson().fromJson(txData, FontSize[].class);
                listData3.addAll(Arrays.asList(arrAlarm));
                Settings.System.putString(getBaseContext().getContentResolver(), Settings.System.FONT_SCALE, listData3.get(listData3.size()-1).getNumB());
                finish();

            }
        });
        dialog.show();
    }
}
