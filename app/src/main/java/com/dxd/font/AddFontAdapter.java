package com.dxd.font;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AddFontAdapter extends RecyclerView.Adapter<AddFontAdapter.AlarmHolder>{

    private Context mContext;
    private final List<FontSize> listFont;
    private CallBack mCallBack;
    public static final String KEY_SHOW_ACT = "KEY_SHOW_ACT";
    private  boolean a = false;

    public void setmCallBack(CallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    public AddFontAdapter(Context mContext, List<FontSize> listFont) {
        this.mContext = mContext;
        this.listFont = listFont;
    }

    @NonNull
    @Override
    public AddFontAdapter.AlarmHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.item_font,parent,false);
        return new AlarmHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddFontAdapter.AlarmHolder holder, int position) {
        FontSize data= listFont.get(position);
        // 100% - Mặc Định (1x)
        holder.tvThongSo.setText(data.getNumA()+"% - ("+data.getNumB()+"x)");
        holder.tvSizeFont.setText(data.getNumC()+"sp");
        a =data.isBlx();
        if (a){
            holder.btAD.setText("Đang dùng");
            holder.btAD.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color1));
        } else {
            holder.btAD.setText("Áp dụng");
            holder.btAD.setBackgroundColor(ContextCompat.getColor(mContext, R.color.purple_711));
        }
        holder.btAD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < listFont.size(); i++) {
                    if (i!= position){
                        listFont.get(i).setBlx(false);
                    } else {
                        listFont.get(i).setBlx(true);
                    }
                }
                mCallBack.actionCallBack(KEY_SHOW_ACT,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listFont.size();
    }

    public class AlarmHolder  extends RecyclerView.ViewHolder{
        TextView tvThongSo,tvSizeFont;
        Button btAD;
        public AlarmHolder(@NonNull View itemView) {
            super(itemView);
            tvThongSo = itemView.findViewById(R.id.tv_thongso);
            tvSizeFont = itemView.findViewById(R.id.tv_sizefont);
            btAD = itemView.findViewById(R.id.bt_apdung);

        }
    }
}
