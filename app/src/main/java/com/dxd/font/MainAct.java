package com.dxd.font;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static com.dxd.font.AddFontAdapter.KEY_SHOW_ACT;

public class MainAct extends AppCompatActivity implements CallBack {
    public static final String KEY_DATA = "KEY_DATA";
    private AddFontAdapter addFontAdapter;
    private List<FontSize> fontSizeList;
    private RecyclerView recyclerView;
    private String a="";
    private TableRow tb_alertText;
    private int dateSS = 0;
    private int monthSS = 0;
    public static final String KEY_VALUE ="KEY_VALUE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        saveFont();
        initViews();
        Log.d("AXX", Settings.System.canWrite(this)+"");

    }

    private void initViews() {
        tb_alertText = findViewById(R.id.tb_text);
        tb_alertText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<FontSize> listData2 = new ArrayList<>();
                String txData = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE).getString(MainAct.KEY_DATA, null);
                FontSize[] arrAlarm = new Gson().fromJson(txData, FontSize[].class);
                listData2.addAll(Arrays.asList(arrAlarm));

                for (int i = 0; i < listData2.size(); i++) {
                   int a = listData2.get(i).getNumA();
                   if (a == 100){
                       listData2.get(i).setBlx(true);
                       Settings.System.putString(getBaseContext().getContentResolver(), Settings.System.FONT_SCALE, listData2.get(i).getNumB());
                   } else {
                       listData2.get(i).setBlx(false);
                   }
                }

                SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
                pref.edit().putString(MainAct.KEY_DATA, new Gson().toJson(listData2)).apply();
            }
        });
        findViewById(R.id.iv_setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainAct.this, Setting_Act.class);
                startActivity(intent);
                finish();
                
            }
        });
        findViewById(R.id.bt_add_font).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainAct.this, AddFont_Act.class);
                startActivity(intent);
            }
        });


        recyclerView = findViewById(R.id.rv_list_font);
        String txData = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE).getString(KEY_DATA, null);
        if (txData == null) return;
        FontSize[] arrAlarm = new Gson().fromJson(txData, FontSize[].class);
        fontSizeList = new ArrayList<>(Arrays.asList(arrAlarm));

        for (int i = 0; i < fontSizeList.size(); i++) {
            boolean axx = fontSizeList.get(i).isBlx();
            int axx1 = fontSizeList.get(i).getNumA();
            if (axx1 == 100 && !axx){
                tb_alertText.setVisibility(View.VISIBLE);
            }
        }

        addFontAdapter = new AddFontAdapter(getApplicationContext(), fontSizeList);
        addFontAdapter.setmCallBack(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(addFontAdapter);

    }
    

    private void saveFont() {
        String txData = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE).getString(MainAct.KEY_DATA, null);
        if (txData== null){
            FontSize fontSize0 = new FontSize(100, "1.0", 22, true, false);
            FontSize fontSize1 = new FontSize(120, "1.2", 24, false, false);
            FontSize fontSize2 = new FontSize(140, "1.4", 26, false, false);
            FontSize fontSize3 = new FontSize(160, "1.6", 28, false, false);
            FontSize fontSize4 = new FontSize(180, "1.8", 30, false, false);
            FontSize fontSize5 = new FontSize(200, "2.0", 32, false, false);
            List<FontSize> listData = new ArrayList<>();
            listData.add(fontSize0);
            listData.add(fontSize1);
            listData.add(fontSize2);
            listData.add(fontSize3);
            listData.add(fontSize4);
            listData.add(fontSize5);
            SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
            pref.edit().putString(MainAct.KEY_DATA, new Gson().toJson(listData)).apply();

        }
    }

    @Override
    public void actionCallBack(String key, Object object) {
        if (key == KEY_SHOW_ACT) {
            int x = (Integer) object;
            List<FontSize> listData1 = new ArrayList<>();
            String txData = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE).getString(MainAct.KEY_DATA, null);
            FontSize[] arrAlarm = new Gson().fromJson(txData, FontSize[].class);
            listData1.addAll(Arrays.asList(arrAlarm));
            if (listData1.get(x).isBlx()){
                return;
            }
            if (Settings.System.canWrite(this)){
                showDialogSuccess(x);
            } else {
                AlertDialog.Builder alDialog = new AlertDialog.Builder(this);
                alDialog.setTitle("Thông báo!");
                alDialog.setIcon(R.mipmap.ic_launcher);
                alDialog.setMessage("Bạn cần cấp quyền cho thay đổi này!");
                alDialog.setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkSystemWritePermission();
                        dialog.dismiss();
                    }
                });
                alDialog.setNegativeButton("Từ chối", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alDialog.show();

            }
        }
    }

    private boolean checkSystemWritePermission() {
        boolean retVal = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            retVal = Settings.System.canWrite(this);
            Log.d("TAG", "Can Write Settings: " + retVal);
            openAndroidPermissionsMenu();
            retVal= true;
            Toast.makeText(this, retVal+"", Toast.LENGTH_SHORT).show();
        }
        return retVal;
    }
    private void openAndroidPermissionsMenu() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + this.getPackageName()));
        Log.i("AXX", Settings.System.canWrite(this)+"");
        startActivity(intent);
    }

    private void showDialogSuccess(int a){
        List<FontSize> list = new ArrayList<>();
        String txData = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE).getString(MainAct.KEY_DATA, null);
        FontSize[] arrAlarm = new Gson().fromJson(txData, FontSize[].class);
        list.addAll(Arrays.asList(arrAlarm));

        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);

        Toast.makeText(this, "ok1", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MainAct.this, NotificationReceiver.class);
        intent.putExtra(KEY_VALUE, list.get(a).getNumA());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainAct.this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager= (AlarmManager) getSystemService(ALARM_SERVICE);
        long i = 7*24*60*60*1000;
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, SystemClock.currentThreadTimeMillis()+i, i, pendingIntent);

        Window window = dialog.getWindow();
        if (window == null){
            return;
        }
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView imBackdialog = dialog.findViewById(R.id.iv_back_dialog);
        imBackdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < list.size(); i++) {
                    if (i==a){
                        list.get(i).setBlx(true);
                    } else {
                        list.get(i).setBlx(false);
                    }
                }
                Settings.System.putString(getBaseContext().getContentResolver(), Settings.System.FONT_SCALE, list.get(a).getNumB());
                SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
                pref.edit().putString(MainAct.KEY_DATA, new Gson().toJson(list)).apply();
                dialog.dismiss();
            }
        });
        dialog.show();
    }


}